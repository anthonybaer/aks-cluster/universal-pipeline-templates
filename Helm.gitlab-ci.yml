# Documentation available at https://helm.sh/docs/helm/helm/

.helm:
  image: ${CONTAINER_PATH}/kubernetes-container:latest
  variables:
    HELM_CHART_DIR: "./Chart.yaml"
    HELM_CHART_VALUES_PATH: "Values.yaml"
    K8S_NAMESPACE: ""
    HELM_RELEASE_NAME: ""
    KUBE_CONTEXT: $KUBE_CONTEXT
    HELM_CLI_OPTS: ""

# set KUBE_CONTEXT
.kube-context: &kube-context
  - kubectl config use-context $KUBE_CONTEXT

.helm-deploy:
  extends: .helm
  script:
    - *kube-context
    - helm upgrade --install $HELM_RELEASE_NAME $HELM_CHART $HELM_CLI_OPTS
  when: manual

.helm-lint:
  extends: .helm
  script:
    - *kube-context
    - helm lint $HELM_CHART_DIR $HELM_CLI_OPTS

.helm-package:
  extends: .helm
  script:
    - helm package $HELM_CHART_DIR $HELM_CLI_OPTS

.helm-publish:
  variables:
    HELM_PACKAGE: "$PACKAGE_NAME"
    HELM_PACKAGE_VERSION: "$VERSION"
    HELM_CHANNEL: "stable"
  script:
    - curl --request POST --form '${HELM_PACKAGE}-${HELM_PACKAGE_VERSION}.tgz' --user GitLab:$CI_JOB_TOKEN $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/helm/api/$HELM_CHANNEL/charts

.helm-push:
  extends: .helm
  variables:
    HELM_PACKAGE: "$PACKAGE_NAME"
    HELM_PACKAGE_VERSION: "$VERSION"
    REMOTE_REGISTRY: "oci://${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/"
  # This job will require a before script to login into the registry where the package is being pushed.
  # Example:
  # before_script:
  #   - aws ecr get-login-password --region "${REGION}" | helm registry login --username "${USERNAME}" --password-stdin "${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com"
  script:
    - *kube-context
    - helm push ${HELM_PACKAGE}-${HELM_PACKAGE_VERSION}.tgz $REMOTE_REGISTRY $HELM_CLI_OPTS

.helm-repo:
  extends: .helm
  image: ${CONTAINER_PATH}/kubernetes-container:latest
  variables:
    HELM_REPO_NAME: "${CI_PROJECT_NAME}"  
    HELM_REG_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/"
    HELM_CHANNEL: "stable"

.helm-repo:add:
  extends: .helm-repo
  script:
    - *kube-context
    - helm repo add $HELM_REPO_NAME $|HELM_REG_URL/$HELM_CHANNEL $HELM_CLI_OPTS

.helm-repo:index:
  extends: .helm-repo
  script:
    - *kube-context
    - helm repo index $HELM_CHART_DIR $HELM_CLI_OPTS

.helm-repo:list:
  extends: .helm-repo
  script:
    - *kube-context
    - helm repo list $HELM_CLI_OPTS
  
.helm-repo:remove:
  extends: .helm-repo
  script:
    - *kube-context
    - helm repo remove $HELM_REPO_NAME $HELM_CLI_OPTS

.helm-repo:update:
  extends: .helm-repo
  script:
    - *kube-context
    - helm repo update $HELM_REPO_NAME $HELM_CLI_OPTS

.helm-rollback:
  extends: .helm
  variables:
    REVISION: ""
  script:
    - *kube-context
    - helm rollback $HELM_RELEASE_NAME $REVISION $HELM_CLI_OPTS
  when: manual

.helm-test:
  extends: .helm
  script:
    - *kube-context
    - helm test $HELM_RELEASE_NAME $HELM_CLI_OPTS

.helm-uninstall:
  extends: .helm
  script:
    - *kube-context
    - helm uninstall $HELM_RELEASE_NAME $HELM_CLI_OPTS
  when: manual
