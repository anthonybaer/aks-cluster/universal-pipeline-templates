# Test

## Maven Testing

### .mvn-test

This job invokes a maven `verify` task. It is used to run any checks to verify the package is valid and meets quality criteria by running all integration tests found in the project. For more information, see [Apache Maven Project | Introduciton to the Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html.)

**Usage**

```yaml
mvn-test:
  extends: .mvn-test
  variables:
    MAVEN_CLI_OPTS: ""
```

**Image**: *custom-registry*/maven-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|MAVEN_CLI_OPTS|''|Used to specify maven cli options.|

---

## Gradle Testing

### .gradle-test

This job invokes a gradle `test` task. This runs a collection of test cases using any supported test library — JUnit, JUnit Platform or TestNG — and collates the results. For more information, see [Gradle | Testing in Java & JVM projects](https://docs.gradle.org/current/userguide/java_testing.html).

**Usage**

```yaml
gradle-test:
  extends: .gradle-test
  variables:
    GRADLE_CLI_OPTS: "--continue"
```

**Image**: *custom-registry*/gradle-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|GRADLE_CLI_OPTS|''|Used to specify greadle cli options.|

---

## Python Testing

### .pytest-test

This invokes [pytest](https://docs.pytest.org/en/7.1.x/contents.html), and outputs a JUnit report file. For more information, see [pytest | Managing pytest’s output | Creating JUnitXML format files](https://docs.pytest.org/en/7.1.x/how-to/output.html?highlight=junitxml#creating-junitxml-format-files)

**Usage**

```yaml
pytest-test:
  extends: .pytest-test
```

**Image**: *custom-registry*/python3-container:latest

**No Variables**

---

## Jest Testing Framework

### .jest-test

This invoked [JEST](https://jestjs.io/). It is running with the [`--ci` option](https://jestjs.io/docs/cli#--ci) so it fails if it encounters a snapshot. It is also running with the [default and junit `--reporters`](https://jestjs.io/docs/cli#--reporters) to output results to a JUnit report file.

**Usage**

```yaml
jest-test:
  extends: .jest-test
```

**Image**: *custom-registry*/nodejs-container:latest

**No Variables**

---

## Selenium Testing

This job's purpose is to spin up a selenium test alongside your application. It will then run selemenium tests against your application using the specifeid web browser. Use `before_script:` to configure your application to run. For more information, see [SeleniumHQ/docker-selenium](https://github.com/SeleniumHQ/docker-selenium).

### .selenium-chrome-test

This job uses the Chrome web browser.

**Usage**

```yaml
selenium-chrome-test:
  extends: .selenium-chrome-test
```

**Image**: [selenium/standalone-chrome](https://hub.docker.com/r/selenium/standalone-chrome)

**No Variables**

### .selenium-firefox-test

This job uses the Firefox web browser.

**Usage**

```yaml
selenium-firefox-test:
  extends: .selenium-firefox-test
```

**Image**: [selenium/standalone-firefox](https://hub.docker.com/r/selenium/standalone-firefox)

**No Variables**

### .selenium-edge-test

This job uses the Edge web browser.

**Usage**

```yaml
selenium-edge-test:
  extends: .selenium-edge-test
```

**Image**: [selenium/standalone-edge](https://hub.docker.com/r/selenium/standalone-edge)

**No Variables**