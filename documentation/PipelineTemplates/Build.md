# Build

Build jobs are part of the `Build.gitlab-ci.yml` template, which will be added/extended in your pipeline through the `Application.gitlab-ci.yml`.


## .gradle-build

This job is used for Gradle builds. It contains the Java JRE and Gradle toolchains. We invoke Gradle via the `gradle $GRADLE_CLI_OPTS assemble` format. For more information, see [Tasks | The Base Plugin](https://docs.gradle.org/current/userguide/base_plugin.html#sec:base_tasks)

**Usage**

```yaml
package-gradle:
  stage: build
  extends: .gradle-build
  variables:
    GRADLE_CLI_OPTS: '--dry-run'
```

**Image**: *custom-registry*/gradle-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|GRADLE_CLI_OPTS|''|Used to specify new build parameters for gradle, before we assemble it.|

---

## .maven-build

This job is used for Maven builds. It contains the Java JRE and Maven toolchains. We invoke Maven via the `mvn $MAVEN_CLI_OPTS package` format. For more information, see [Maven | Introduction to the Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)

**Usage**

```yaml
package-maven:
  stage: build
  extends: .maven-build
  variables:
    MAVEN_CLI_OPTS: '-X'
```

**Image**: *custom-registry*/maven-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|MAVEN_CLI_OPTS|''|Used to specify new build parameters for maven, before we assemble it.|

---

## .npm-build

This job invokes `npm install` and then `npm rebuild`. The `npm install` command exists in a `before_script` block, this means the user can overwrite the `before_script` block to set env variables and other items. It is by design that the `before_script` block be overwritten. We invoke NPM via the `npm install` and `npm rebuild $NPM_CLI_OPTS` formats. For more information, see [npm-install](https://docs.npmjs.com/cli/v8/commands/npm-install) & [npm-rebuild](https://docs.npmjs.com/cli/v8/commands/npm-rebuild)

**Usage**

```yaml
package-npm:
  stage: build
  extends: .npm-build
  variables:
    NPM_CLI_OPTS: '--ignore-scripts'
```

**Image**: *custom-registry*/nodejs-container

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|NPM_CLI_OPTS|''|Used to specify new build parameters for npm, before we assemble it.|

---

## .yarn-build

This job invokes `npm -g install yarn` then, `yarn install`, and then `npm build`. The `yarn install` command exists in a `before_script` block, this means the user can overwrite the `before_script` block to set env variables and other items. It is by design that the `before_script` block be overwritten. However any superceding `before_script` block should contain the `npm -g install yarn` and `yarn install` commands. More information available at [yarn.BUILD](https://yarn.build/)

**Usage**

```yaml
package-yarn:
  stage: build
  extends: .yarn-build
```

**Image**: *custom-registry*/nodejs-container

**No Variables**

---

## .go-build

This job is used for Go builds. It contains the Go toolchain. We invoke Go via the `go build $GO_CLI_OPTS` format. For more information, see [Go Command Library | Compile packages and dependencies](https://pkg.go.dev/cmd/go#hdr-Compile_packages_and_dependencies)

**Usage**

```yaml
package-go:
  stage: build
  extends: .go-build
  variables:
    GO_CLI_OPTS: '-n'
```

**Image**: *custom-registry*/go-container

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|GO_CLI_OPTS|''|Used to specify new build parameters for go, before we assemble it.|

---

## .docker-build

The docker-build job invokes a Buildah Container that is used to build a Docker image. <!-- In the `before_script` block we setup the credentials to login to the GitLab Container Registry. If you want to specify an alternative registry, or an additional registry. You'll need to overwrite the `before_script` section.  -->

The resulting Docker image that is built follows a pattern of `$REGISTRY_IMAGE${tag}` Where the REGISTRY_IMAGE is what is defined by GitLab. $Tag is either `latest`, if ran on a default branch, or the branch name that the pipeline is being ran against.

**Usage**

```yaml
container-build:
  stage: build
  extends: .docker-build
  variables:
    DOCKERBUILD_DIR: '$CI_PROJECT_DIR'
    DOCKERFILE_NAME: 'Dockerfile'
    REGISTRY: $CI_REGISTRY
    REGISTRY_USER: $CI_REGISTRY_USER
    REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
    IMAGE_PATH: ''
```

**Image**: *custom-registry*/buildah-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|DOCKERBUILD_DIR|'$CI_PROJECT_DIR'|Used to specify which directory your dockerfile resides in, and where to do the docker build.|
|DOCKERFILE_NAME|'Dockerfile'|Used to specify which Dockerfile to build.|
|REGISTRY|'$CI_REGISTRY'|Used to specify a container registry, if not using GitLab Container Registry.|
|REGISTRY_USER|'$CI_REGISTRY_USER'|Used to specify a container registry user, if not using GitLab Container Registry.|
|REGISTRY_PASSWORD|'$CI_REGISTRY_PASSWORD'|Used to specify a container registry token/password, if not using GitLab Container Registry.|
|IMAGE_PATH|''|Used to specify the path to the container image, if not using GitLab Container Registry.|
