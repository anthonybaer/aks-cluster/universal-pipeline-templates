# Quick Start Guide

This quick start is going to talk about branching strategies and demonstrate how to set up a project, get your source code in place, write your CI/CD configuration file (.gitlab-ci.yml), build, and deploy/deliver your application.

Assumptions:
- You have an understanding of [git branching strategies](https://www.flagship.io/git-branching-strategies/)
- You have access to functional source code
- You have a working knowldge of [git](https://git-scm.com/)
- You have a basic understanding of [Continuous Integration/Continuous Delivery/Deployment (CI/CD)](https://about.gitlab.com/topics/ci-cd/)

## Branching

Understanding git branching strategies is key to the successful automation of CI/CD piepleines. There are several approaches with different pros and cons that can be considered. To leverage the Pipeline COE, you will need to be leveraging a model that has specific branches for CI and other branches for CD. Essentially, development happens on a feature branch and gets merged to a long-lived branch to be deployed.

We **highly recommend** using `GitLab Flow`, as GitLab is optimized for the strategy. The 3 branching strategies that are supported by the Pipeline COE are GitLab Flow, GitHub Flow, and GitFlow. Take a look at the following diagrams and decide which branching strategy you are currently using, or are aspiring to use.

> [!NOTE]
> For branch naming, `main` and `master` will be used interchangeably.  
> The term `feature` branch will be use generically to mean any branch that is not deploying to a static environment and is temporary. I.E. `feature`, `bugfix`, `hotfix`, `expiremental`, `wip`.

<!-- tabs:start -->

#### **GitLab Flow**
[GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) can support Environment branches and Release branches. We are going to focus on the Environment branches and use the [GitLab Releases](https://docs.gitlab.com/ee/user/project/releases/) functionality. 

GitLab Flow is a simpler alternative to GitFlow that combines feature-driven development and feature branching with issue tracking.

With GitFlow, developers create a develop branch and make that the default while GitLab Flow works with the main branch right away.

GitLab Flow is great when you want to maintain multiple environments and when you prefer to have a staging environment separate from the production environment. Then, whenever the main branch is ready to be deployed, you can merge back into the production branch and release it.

Thus, this strategy offers proper isolation between environments allowing developers to maintain several versions of software in different environments.

While GitHub Flow assumes that you can deploy into production whenever you merge a feature branch into the master, GitLab Flow seeks to resolve that issue by allowing the code to pass through internal environments before it reaches production, as seen in the image below.

**Environment branches with GitLab flow**

<img src="img/gitlab-flow.drawio.svg" title="Environment branches with GitLab flow">

#### **GitHub Flow**

[GitHub Flow](https://githubflow.github.io/) is a simpler alternative to GitFlow ideal for smaller teams as they don’t need to manage multiple versions.

Unlike GitFlow, this model doesn’t have release branches. You start off with the main branch then developers create branches, feature branches that stem directly from the master, to isolate their work which are then merged back into main. The feature branch is then deleted.

The main idea behind this model is keeping the master code in a constant deployable state and hence can support continuous integration and continuous delivery processes
<img src="img/github-flow.drawio.svg">

#### **GitFlow**

[GitFlow](https://nvie.com/posts/a-successful-git-branching-model/) enables parallel development where developers can work separately from the master branch on features where a feature branch is created from the master branch.

Afterwards, when changes are complete, the developer merges these changes back to the master branch for release.

This branching strategy consists of the following branches:

- Master 
- Develop
- Feature- to develop new features that branches off the develop branch 
- Release- help prepare a new production release; usually branched from the develop branch and must be merged back to both develop and master
- Hotfix- also helps prepare for a release but unlike release branches, hotfix branches arise from a bug that has been discovered and must be resolved; it enables developers to keep working on their own changes on the develop branch while the bug is being fixed.

The main and develop branches are considered to be the main branches, with an infinite lifetime, while the rest are supporting branches that are meant to aid parallel development among developers, usually short-lived.

<img src="img/git-flow.png">

<!-- tabs:end -->

## Setting up your project

Protecting your source code and deployment model is the main idea for this section. We want to ensure that only certain people or groups are allowed to impact how our code is modified, merged, and deployed. To ensure that your application is compiled and deployed in a secure and compliant fashion, we are going set some project configurations. There a couple key concepts that we will leverage. 

We want to set up our project so we have control over what gets built and deployed. This means we need to ensure that the right things are built and deploy, to the correct environment, and are approved by the right people. 

The flow of the development cycle will look something like this:

<img src="img/gitlab-flow-Developer Workflow.drawio.svg">

<!-- tabs:start -->

#### **Roles**

Users have different abilities depending on the role they have in a particular group or project. If a user is both in a project’s group and the project itself, the highest role is used. To get a deep understanding for the permissions assocaited to each role, see [Permissions and roles](https://docs.gitlab.com/ee/user/permissions.html).

For setting up your project you need to be an `owner`.

We will set controls for your project around the `developer`, `maintainer`, and `owner` roles.

> [!ATTENTION]
> The person creating the project will be given the `owner` role by default. All work below should be done by a user with the `owner` role for the project.

#### **Protected Branches**

[Protecting branches](https://docs.gitlab.com/ee/user/project/protected_branches.html) imposes restrictions not associated to roles. When a branch is protected, you can limit would can push to it and who can force push to it. Also, a protected branch cannot be deleted. Only `maintainers` and `owners` can remove the protection from a branch.

#### **Merge Requests**

[Merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/) can be configured to require approval before changes can be merged. The approval rules can include individual users or groups, code owwers, users with specific permissions to approve work.

#### **Protected Environments**

[Environments](https://docs.gitlab.com/ee/ci/environments/index.html) can be used for both testing and production reasons.

Because deploy jobs can be raised by different users with different roles, it’s important to be able to protect specific environments from the effects of unauthorized users.

By default, a protected environment ensures that only people with the appropriate privileges can deploy to it, keeping the environment safe.

> [!NOTE]
> We will be using 3 protected environments in this walkthrough; `production`, `staging`, and `development`.

#### **Compliance Framework**

**Ultimate License Only**

[Compliance frameworks](https://docs.gitlab.com/ee/user/group/manage.html#compliance-frameworks) are used to enforce certain compliance requirements or standards.

The [Compliance pipelines](https://docs.gitlab.com/ee/user/group/manage.html#configure-a-compliance-pipeline) define a pipeline configuration to run for any projects with a given compliance framework.

<!-- tabs:end -->

### Let's begin

1. [Create a project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project) in GitLab by [importing](https://docs.gitlab.com/ee/user/project/import/index.html), using a [template](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project-from-a-custom-template), or a [blank project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-blank-project) for you to push your repository to.
1. Let's set the [default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project) based on the table below.

    | Branching Strategy | Default Branch |
    | - | - |
    | GitLab Flow | `main` |
    | GitHub Flow | `main` |
    | GitFlow | `development` or `dev` |

1. We are going to set protected branches. Use the GitLab documentation on how to [configure a protected branch](https://docs.gitlab.com/ee/user/project/protected_branches.html#configure-a-protected-branch).
    <!-- tabs:start -->

    #### **GitLab Flow**

    For this branching strategy, there will be at least 2 branches that are protected. In theory, there should be a branch for each environment you are deploying to. For this example, we will have 3 protected branches for 3 environments. All feature branches will be created from the `main`(default) branch.

    The branch to environment mapping is:
    | Branch | Environment |
    |-|-|
    | main (default branch) | development |
    | staging | staging |
    | prod | production |

    #### **GitHub Flow**

    For this branching strategy, there will be 1 branch that is protected, `main` and it is be your default branch. All feature branches will be created from this branch.

    The branch to environment mapping is:
    | Branch | Environment |
    |-|-|
    | main (default branch) | development, staging, and production |

    #### **GitFlow**

    For this branching strategy, there will be 2 branches that are protected. All feature branches will be created from the `dev` branch.

    The branch to environment mapping is:
    | Branch | Environment |
    |-|-|
    | dev | development |
    | main (default branch) | staging and production |

    <!-- tabs:end -->

1. To set up the merge request approvals, we are going to create the approval rule and and set approval settings.
    -  Following the [documentation](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html#add-an-approval-rule) to create a rule:
        - `Rule name` should be a human readable name of your choosing. i.e. `Deployment Approvers`
        - Set `Target branch` to `All protected branches`
        - Set `Approvers required` to `1`.
        - In the `Add approvers` section add yourself. This can be updated in the future to add a GitLab group or individual users to be able to approve merge requests.
        - Click `Add approval rule`
    - Move down to `Approval settings`
        - Check `Prevent approval by author`, `Prevent approvals by users who add commits`, and `Prevent editing approval rules in merge requests`
        - Uncheck `Require user password to approve`
    - Click `Save changes`
    
1. To create and set the protected environments, first we need to create environments.
    - Following the [documentation](https://docs.gitlab.com/ee/ci/environments/#create-a-static-environment), create 3 static environments: `production`, `staging`, and `development`.
    - Following the [documentation](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#protecting-environments), protect the environments you just created with the following settings:

    | Environment | Allowed to deploy | Required approvals |
    |-|-|-|
    | production | Maintainers | 1 |
    | staging | Maintainers | 1 |
    | development | Developers + Maintainers | 1 |

1. **Ultimate License Only** Compliance Framework is set per project. Add a Compliace Framework to your project following the [documentation](https://docs.gitlab.com/ee/user/project/settings/#add-a-compliance-framework-to-a-project).

**Congrats on setting up your project!!!** Now that we have your project all set up, take a break and grab some coffee. When you are ready, let's start building your pipleline!

## Creating your pipeline

To get started, we will create a sample pipeline jobs in the Pipeline COE library. This will not impact your code or deploy any resources. This pipeline is only designed to demostrate the capabilities and is not a production solution.

1. We are going to work in the [Pipeline Editor](https://docs.gitlab.com/ee/ci/pipeline_editor/) to build your `.gitlab-ci.yml` file. To access the editor, go to **CI/CD > Editor**

1. If you want to work on a branch that is not the default branch, select it from the drop down in the top left of the pipeline editor pane.

1. Once in the editor, we are going to add a few lines to include the Pipeline COE library, using [`include`](https://docs.gitlab.com/ee/ci/yaml/#include). The location should be available in the [`Application.gitlab-ci.yml`](/PipelineTemplates/Application) documentation.

    ```yaml
    include:
      - project: pipelinecoe/pipeline-templates
        ref: main
        file: 'Application.gitlab-ci.yml'
    ```

1. Now that we have included the library, we are going to add a few job templates by using [`extends`](https://docs.gitlab.com/ee/ci/yaml/#extends). You can add one or all of these jobs.

    <!-- tabs:start -->

    #### **Hello World**

    This job template is very simple, it prints out a few lines via `echo`. All you have to do is extend the job and it gets included in your pipeline.

    <!-- tabs:start -->

    #### **Add**

    By adding these 3 lines to your `.gitlab-ci.yml` file, you are including funtionality of the `.hello-world` job from the Pipeline COE library.

    ```yaml
    hello-world:            # The name you give the job in your pipeline. It can be anything within the Job name limitations(https://docs.gitlab.com/ee/ci/jobs/#job-name-limitations).
      extends: .hello-world # The job that is being extended from the Pipeline COE library.
      stage: build          # The stage in which the job will run
    ```

    #### **What's included**

    This is the job that is being extended. It can be found in the `Sample.gitlab-ci.yml` file.

    ```yaml
    .hello-world:   # The extended job.
      script:
        - echo "Welcome to GitLab CI/CD!"
        - echo "Thanks for adding me to your pipeline"
    ```

    <!-- tabs:end -->

    #### **Hello Project**

    This job template is a little different as it uses one of GitLab's [Predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html), `CI_PROJECT_NAME`.

    <!-- tabs:start -->

    #### **Add**

    By adding these 3 lines to your `.gitlab-ci.yml` file, you are including funtionality of the `.hello-project` job from the Pipeline COE library.

    ```yaml
    hello-project:              # The name you give the job in your pipeline. It can be anything within the Job name limitations(https://docs.gitlab.com/ee/ci/jobs/#job-name-limitations).
      extends: .hello-project   # The job that is being extended from the Pipeline COE library.
      stage: build              # The stage in which the job will run
    ```

    #### **What's included**

    This is the job that is being extended. It can be found in the `Sample.gitlab-ci.yml` file.

    ```yaml
    .hello-project: # The extended job.
      script:
        - echo "$CI_PROJECT_NAME is pretty awesome!"
    ```

    <!-- tabs:end -->

    #### **Hello Custom**

    This job template gives you the option to control the output by passing a [variable](https://docs.gitlab.com/ee/ci/variables/), `TEXT`. If you do not pass a variable, it will assume the default in the extended job.

    <!-- tabs:start -->

    #### **Add**

    By adding these lines to your `.gitlab-ci.yml` file, you are including funtionality of the `.hello-custom` job from the Pipeline COE library.

    ```yaml
    hello-custom:                               # The name you give the job in your pipeline. It can be anything within the Job name limitations(https://docs.gitlab.com/ee/ci/jobs/#job-name-limitations).
      extends: .hello-custom                    # The job that is being extended from the Pipeline COE library.
      stage: build                              # The stage in which the job will run
      variables:                                # The section where you set variables
        TEXT: "Add whatever test you'd like"    # The value you want to pass in the variable
    ```

    #### **What's included**

    This is the job that is being extended. It can be found in the `Sample.gitlab-ci.yml` file.

    ```yaml
    .hello-custom:                  # The extended job.
      variables:
        TEXT: "You can replace me." # The default value for the variable.
      script:
        - echo "Pass me a variable, I'm open!"
        - echo "You passed me - $TEXT"
    ```

    <!-- tabs:end -->

    <!-- tabs:end -->

1. Let's commit the changes to our `.gitlab-ci.yml` file and see our pipeline.
    1. Near the bottom, in the `Commit message` box, add a useful message. i.e. "Initial commit to create .gitlab-ci.yml file."
    1. Click the `Commit changes' button.

1. Navigate to **CI/CD > Pipelines** to view the pipeline that is running. 
    1. To view the jobs, click on the button that indicates the status of your pipeline.
    1. If you are using the Compliance Framework, you will see stages and jobs added to your pipeline.
    1. To view the output of each job, click on the job name.

**Congratulations!!** You just ran your first pipeline. You are well on your way to building awesome pipelines.

## Delivery and Deployment

----------**AUTOMATION**----------

